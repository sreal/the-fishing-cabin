﻿using UnityEngine;
using System.Collections;

public class RodMovement : MonoBehaviour {

    Vector3 MousePrev = new Vector3();

    GameObject EmptyLine;
    GameObject FullLine;
    GameObject FishingLine;

    GameObject ClueOne;
    GameObject Biting;

    float MaxRange = 2;

    Vector3 pulledInPosition;
    Vector3 castPosition;
    Vector3 castRotationDelta;
    Vector3 pulledInRotationDelta;

    BitingFish fish;

    bool isFishing = true;
    public float castedX = 0;
    public float castedY = 0;
    float minX = -1;
    float maxX = 1;
    float minY = -1;
    float maxY = 1;

    void Awake() {
        EmptyLine = transform.Find("EmptyLine").gameObject;
        FullLine = transform.Find("FullColorLine").gameObject;
        FishingLine = transform.Find("FishingLine").gameObject;
        ClueOne = transform.Find("RodClueOne").gameObject;
        Biting = transform.Find("RodBiting").gameObject;

        fish = GetComponent<BitingFish>();

        pulledInPosition = new Vector3(5.6f, -3.0f, -5.5f);
        pulledInRotationDelta = new Vector3(0f, 0.0f, 26.5f);

        castPosition = new Vector3(5.6f, -8.0f, 0f);
        castRotationDelta = new Vector3(0f, 0.0f, -26.5f);
    }

    void Start () {
        MousePrev = Input.mousePosition;
    }

    void Update () {
        if (Input.GetButton("Fire1"))
            EnsureRodIsCasted();
        else
            EnsureRodIsPulledIn();

        ClueOne.SetActive(isFishing && fish.ShowClueOne);
        Biting.SetActive(isFishing && fish.IsBiting);

        FollowMouse();
    }

    void UpdateRange()
    {
        minY = transform.position.y - MaxRange;
        maxY = transform.position.y + MaxRange;
        minX = transform.position.x - MaxRange;
        maxX = transform.position.x + MaxRange;
    }
    void EnsureRodIsCasted()
    {
        if (!isFishing) {
            transform.Rotate(castRotationDelta);
            transform.position = castPosition;
            UpdateRange();
            fish.StartFishing();
        }
        FullLine.SetActive(false);
        EmptyLine.SetActive(false);
        FishingLine.SetActive(true);
        isFishing = true;
    }
    void EnsureRodIsPulledIn()
    {
        if (isFishing) {
            transform.position = pulledInPosition;
            transform.Rotate(pulledInRotationDelta);
            UpdateRange();
            fish.StopFishing();

            if (fish.IsBiting) {
                var color = FullLine.transform.Find("FullLineColor").gameObject;
                color.GetComponent<SpriteRenderer>().color = BitingFish.GetFishColor();
            }
            FullLine.SetActive(fish.IsBiting);
        }
        EmptyLine.SetActive(true);
        FishingLine.SetActive(false);
        isFishing = false;
    }

    void FollowMouse()
    {
        var mouseDampener = Time.deltaTime * 0.1f;
        var mouseDelta = (Input.mousePosition - MousePrev);
        MousePrev = Input.mousePosition;

        if (mouseDelta.magnitude > 50) { return; }
        mouseDelta.Scale(new Vector3(mouseDampener, mouseDampener, 1));

        var newX = transform.position.x;
        var newY = transform.position.y;
        if (transform.position.y + mouseDelta.y <= minY) {
            newY = minY;
        } else if (transform.position.y + mouseDelta.y > maxY) {
            newY = maxY;
        } else {
            newY = transform.position.y + mouseDelta.y;
        }
        if (transform.position.x + mouseDelta.x <= minX) {
            newX = minX;
        } else if (transform.position.x + mouseDelta.x > maxX) {
            newX = maxX;
        } else {
            newX = transform.position.x + mouseDelta.x;
        }
        transform.position = new Vector3(newX, newY, transform.position.z);
    }
}
