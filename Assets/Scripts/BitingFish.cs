﻿using UnityEngine;
using System.Collections;

public class BitingFish : MonoBehaviour {

    public bool ShowClueOne = false;
    public bool IsBiting = false;

    void Awake() {
    }

    public static Color GetFishColor()
    {
        return Random.ColorHSV();
    }
    public void StartFishing()
    {
        ShowClueOne = false;
        IsBiting = false;

        StopFishing();
        Debug.Log("StartFishing");
        var startTime = Random.Range(2.0f, 5.0f);
        InvokeRepeating("StartBiting", startTime, 10.0f);
    }
    public void StopFishing()
    {
        Debug.Log("StopFishing");
        StopAllCoroutines();
        CancelInvoke();

    }
    void StartBiting()
    {
        StartCoroutine(Bite());
    }

    IEnumerator Bite() {
        Debug.Log("Bite");
        var delay = Random.Range(0.5f, 2.5f);
        IsBiting = false;
        ShowClueOne = false;
        yield return ToggleClueOne();
        yield return new WaitForSeconds(delay);
        yield return ToggleClueOne();
        yield return new WaitForSeconds(delay);
        yield return ToggleIsBiting();
    }

    IEnumerator ToggleClueOne() {
        ShowClueOne = true;
        yield return new WaitForSeconds(1);
        ShowClueOne = false;
    }
    IEnumerator ToggleIsBiting() {
        IsBiting = true;
        yield return new WaitForSeconds(.5f);
        IsBiting = false;
    }



}
